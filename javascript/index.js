var net = require("net");
var JSONStream = require('JSONStream');
var _ = require("underscore");

var Config = require("./quiborgue/bot-config");
var Bot = require("./quiborgue/bot");

// Para alterar a política de IA modifice o local do arquivo abaixo
var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

var conf = new Config();
var track = conf.get('track');

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);
console.log("Using listeners %s on track %s", conf.get('listeners'), conf.get('track'));
var joinObj = {
  msgType: "join",
  data: {
    name: botName,
    key: botKey
  }
}

if (track !== "join") {
  joinObj.msgType = "createRace";
  joinObj.data = {
    botId: {
      name: botName,
      key: botKey  
    },
    trackName: track
  }
}

client = net.connect(serverPort, serverHost, function() {
  return send(joinObj);
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

var bot1 = new Bot( send );

_.each(conf.get('listeners'), function(listener) {
  bot1.addListener(require(__dirname + "/quiborgue/listeners/" + listener));
});

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
  bot1.process(data);
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
