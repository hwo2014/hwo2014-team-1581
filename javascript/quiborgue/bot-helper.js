var fs = require("fs");
var _ = require("underscore");

module.exports = function(chatterbox) {
	
	this.chatterbox = chatterbox;

	/**
	 * Verifica se o chatterbox foi setado para a politica e obtem-o
	 */
	this.getChatterbox = function() {
		if (this.chatterbox === null) {
			throw "Chatterbox not set. Please execute policy.chatterbox = chatterbox";
		}

		return this.chatterbox;
	};
	
	/**
	 * Obtem informacoes da pista atual
	 */
	this.getTrack = function() {
		return this.getChatterbox().track;
	};


	/**
	 * Obtem informacoes do meu carro
	 */
	this.getCar = function() {
		return this.getChatterbox().car;
	};
	 
	/**
	 * Obtem posicoes dos carros (carros de todos na pista)
	 */
	this.getCarPositions = function() {
		return this.getChatterbox().carPositions;
	};
	
	/**
	 * Obtem posicao do meu carro
	 */
	this.getMyCarPosition = function() {
		var that = this;
		return _.find(this.getCarPositions(), function(carPosition) {
			return carPosition.car.equals(that.getCar());
		});
	};
	
	/**
	 * Obtem pedaço da pista na localização 'index'
	 */
	this.getPiece = function(index) {
		index = index % this.getTrack().pieces.length;
		
		return this.getTrack().pieces[index];
	};
	
	/**
	 * Obtem uma determinada faixa da pista
	 */
	this.getLane = function(index) {
		return _.find(this.getTrack().lanes, function(lane) {
			return lane.index === index;	
		});
	};
}