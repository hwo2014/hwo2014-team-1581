var _ = require("underscore");
_.str = require("underscore.string");

var TrackPiece = require("./track-piece");
var Car = require("./car");

module.exports = function(data) {
	this.id = data.race.track.id;
	this.name = data.race.track.name;
	this.pieces = _.map(data.race.track.pieces, function(piece, index) {
		return new TrackPiece(piece, index);
	});
	this.lanes = data.race.track.lanes;
	this.cars = _.map(data.race.cars, function(car) {
		// TODO: Colocar demais informacoes do carro.
		return new Car(car.id);
	});
	this.laps = data.race.raceSession.laps;
};