var _ = require("underscore");
_.str = require("underscore.string");

module.exports = function(data, index) {
	this.length = null;
	this.switch = false;
	this.radius = 0.0;
	this.angle = 0.0;
	this.index = index;

	if (_.has(data, "length")) {
		this.length = data.length;
	}

	if (_.has(data, "switch")) {
		this.switch = data.switch;
	}

	if (_.has(data, "radius")) {
		this.radius = data.radius;
	}

	if (_.has(data, "angle")) {
		this.angle = data.angle;
	}
};