var _ = require("underscore");
_.str = require("underscore.string");

var Car = require("./car");

module.exports = function(data, gameTick) {
	this.car = new Car(data.id);
	this.angle = data.angle;
	this.pieceIndex = data.piecePosition.pieceIndex;
	this.pieceDistance = data.piecePosition.inPieceDistance;
	this.startLaneIndex = data.piecePosition.lane.startLaneIndex;
	this.endLaneIndex = data.piecePosition.lane.endLaneIndex;
	this.lap = data.piecePosition.lap;
	this.tick = gameTick;
};