module.exports = function(data) {
	this.team = data.name;
	this.color = data.color;

	this.equals = function(car) {
		return (this.team == car.team && this.color == car.color);
	}
};