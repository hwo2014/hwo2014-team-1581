require 'mina/git'

set :user, "ubuntu"
set :domain, 'git.quiborgue.com'
set :deploy_to, '/home/ubuntu/hwo'
set :repository, 'git@git.quiborgue.com:extras/hwo2014.git'
set :branch, 'master'
set :identity_file, ENV['HOME'] + '/.ssh/keyborgue.pem'

task :environment do
  
end

# Put any custom mkdir's in here for when `mina setup` is ran.
# For Rails apps, we'll make some of the shared paths that are shared between
# all releases.
task :setup => :environment do

end

desc "Deploys the current version to the server."
task :deploy => :environment do
  deploy do
    invoke :'git:clone'
  end
end

# For help in making your deploy script, see the Mina documentation:
#
#  - http://nadarei.co/mina
#  - http://nadarei.co/mina/tasks
#  - http://nadarei.co/mina/settings
#  - http://nadarei.co/mina/helpers

