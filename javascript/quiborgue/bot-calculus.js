var _ = require("underscore");

if (typeof(Number.prototype.toRad) === "undefined") {
  Number.prototype.toRad = function() {
    return this * Math.PI / 180;
  }
}

if (typeof(Number.prototype.toDeg) === "undefined") {
  Number.prototype.toDeg = function() {
    return this * 180 / Math.PI;
  }
}

if (typeof(Number.prototype.sign) === "undefined") {
  Number.prototype.sign = function() {
    return this ? this < 0 ? -1 : 1 : 0;
  }
}

module.exports = function() {
	/**
	 * Calculo de um segmento circular é: r * ang em radianos
	 */
	this.trackLength = function(trackPiece, laneDistance) {
		if (trackPiece.length) {
			return trackPiece.length;
		}
		
		if (!laneDistance) {
			laneDistance = 0.0;
		}
		
		var r = trackPiece.radius + (laneDistance * trackPiece.angle.sign() * -1);
		var ang = trackPiece.angle.toRad();
		
		return Math.abs(r * ang);
	};
	
	
	this.curveSpeed = function(trackPiece, laneDistance) {
		if (!trackPiece.angle) {
			return 125;
		}
		
		return this.trackLength(trackPiece, laneDistance);
	};
	
	this.fullLapTimes = function(history) {
		var lapEndTick = [], lapTimes = [];
		
		_.each(history, function(carPosition) {
			lapEndTick[carPosition.lap] = carPosition.tick;		
		});

		_.each(lapEndTick, function(tick, index) {
			if (index == 0) {
				lapTimes[index] = tick/60.0;
			}
			else {
				lapTimes[index] = (tick - lapEndTick[index - 1])/60.0;
			}
		});
		
		return lapTimes;
	};
};