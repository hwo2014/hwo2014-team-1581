var Config = require("../bot-config");
var conf = new Config();

module.exports = {
  database: conf.get('database'),
  directory: './migrations',
  tableName: 'migrations'
};