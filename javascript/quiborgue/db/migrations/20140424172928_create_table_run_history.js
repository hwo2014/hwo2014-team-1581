
exports.up = function(knex, Promise) {
  return knex.schema.createTable('run_history', function (table) {
    table.increments('id').unsigned().primary();
    
    table.float('angle');
    table.float('piece_distance');
    table.integer('start_lane');
    table.integer('end_lane');
    table.integer('lap');
    table.integer('tick');
    
    table.string('run_id').references('id').inTable('run');
    table.integer('piece_id').unsigned().references('id').inTable('track_piece');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('run_history');
};
