
exports.up = function(knex, Promise) {
  return knex.schema.createTable('run', function (table) {
    table.string('id').primary();
    
    table.string('track_id').references('id').inTable('track');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('run');
};