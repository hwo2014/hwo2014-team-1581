
exports.up = function(knex, Promise) {
  return knex.schema.createTable('track_piece', function (table) {
    table.increments('id').unsigned().primary();
    
    table.float('index');
    table.float('length').nullable();
    table.float('angle').nullable();
    table.float('radius').nullable();
    table.float('switch').nullable();
    
    table.string('track_id').references('id').inTable('track');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('track_piece');
};
