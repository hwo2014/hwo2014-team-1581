
exports.up = function(knex, Promise) {
  return knex.schema.createTable('track', function (table) {
    table.string('id').primary();
    
    table.string('name');
    table.string('lanes_json');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('track');
};
