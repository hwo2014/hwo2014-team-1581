var _ = require("underscore");
_.str = require("underscore.string");
_.mixin(_.str.exports());

var Chatterbox = require("./bot-chatterbox");

module.exports = function(send) {
	this.listeners = [];
	var that = this;
	this.chatterbox = new Chatterbox(function(json) {
		that.sentOnListeners(json);
		send(json);
	});

	this.sentOnListeners = function(json) {
		_.each(this.listeners, function(listener) {
			if (_.isFunction(listener['sent'])) {
				listener.sent(json);
			}
		});
	}

 	this.addListener = function(listener) {
		this.listeners.push(new listener(this.chatterbox));
	}

	this.callOnListeners = function(methodName) {
		_.each(this.listeners, function(listener) {
			if (_.isFunction(listener[methodName])) {
				listener[methodName]();
			} else {
				listener['onDefault']();
			}
		});
	};

	this.process = function(message) {
		this.chatterbox.parse(message);
		var func = this.chatterbox.getTypeCall();
		this.callOnListeners(func);
	};

};