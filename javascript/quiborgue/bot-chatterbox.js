var _ = require("underscore");
_.str = require("underscore.string");

var Car = require("./models/car");
var Track = require("./models/track");
var CarPosition = require("./models/car-position");

module.exports = function(send) {
	// Deve receber uma função para ser usada no envio de dados
	this.send = send;

	this.msg = null;
	this.gameTick = null;
	this.car = null;
	this.track = null;
	this.carPositions = null;
	this.carCrashed = null;
	this.carFinished = null;
	this.carDnf = null;
	this.runId = null;

	this.parse = function(message) {
		this.msg = message;
		this.parseGameTick();
		
		this.parseCar();
		this.parseTrack();
		this.parseCarPositions();
		this.parseCarCrashed();
		this.parseCarSpawned();
		this.parseCarFinished();
		this.parseCarDnf();
	};

	this.isMsgType = function(type) {
		return (this.msg.msgType.toLowerCase() == type.toLowerCase());
	};

	this.parseGameTick = function() {
		this.gameTick = null;
		if (_.has(this.msg, "gameTick")) {
			this.gameTick = this.msg.gameTick;	
		}
	};
 
	this.parseCar = function() {
		if (!this.isMsgType("yourcar")) return;
		this.car = new Car(this.msg.data);
	};

	this.parseTrack = function() {
		if (!this.isMsgType("gameinit")) return;
		this.track = new Track(this.msg.data);
	};

	this.parseCarPositions = function() {
		var that = this;
		this.carPositions = null;
		if (!this.isMsgType("carpositions")) return;

		this.carPositions = _.map(this.msg.data, function(carPosition) {
			return new CarPosition(carPosition, that.gameTick);
		});
	};

	this.parseCarCrashed = function() {
		this.carCrashed = null;
		if (!this.isMsgType("crash")) return;
		this.carCrashed = new Car(this.msg.data);
	};

	this.parseCarSpawned = function() {
		this.carSpawned = null;
		if (!this.isMsgType("spawn")) return;
		this.carSpawned = new Car(this.msg.data);
	};

	this.parseCarFinished = function() {
		this.carFinished = null;
		if (!this.isMsgType("finish")) return;
		this.carFinished = new Car(this.msg.data);
	};

	this.parseCarDnf = function() {
		this.carDnf = null;
		if (!this.isMsgType("dnf")) return;
		this.carDnf = new Car(this.msg.data);
	};

	this.getTypeCall = function() {
		return "on" + _(this.msg.msgType).capitalize();
	};

	this.appendGameTick = function(response) {
		if (this.gameTick) {
			response.gameTick = this.gameTick;
		}

		return response;
	};

	this.ping = function() {
		// console.log("> %s ping", this.gameTick);
		this.send(
			this.appendGameTick({
				msgType: "ping",
				data: null
			})
		);
	};

	this.throttle = function(value) {
		// console.log("> %s throttle(%s)", this.gameTick, value);
		this.send(
			this.appendGameTick({
				msgType: "throttle",
				data: value
			})
		);
	};

	this.switchLane = function(value) {
		// console.log("> %s switch lane(%s)", this.gameTick, value);
		this.send(
			this.appendGameTick({
				msgType: "switchLane",
				data: value
			})
		);
	};

	this.turbo = function(value) {
		if (!value) {
			value = "TURBO!";
		}

		// console.log("> %s turbo(%s)", this.gameTick, value);
		this.send(
			this.appendGameTick({
				msgType: "turbo",
				data: value
			})
		);
	}
};