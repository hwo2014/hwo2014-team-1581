var _ = require("underscore");
_.str = require("underscore.string");

var Config = require("./bot-config");
var Knex = require("knex");
var conf = new Config();
Knex.knex = Knex.initialize(conf.get('database'));

// elsewhere, to use the client:
var knex = require('knex').knex;

module.exports = function() {
	this.save = function(table, element, primary, callbackId) {
		if (!primary) {
			primary = 'id';
		}

		if (!_.isFunction(callbackId)) {
			callbackId = function(id) {};
		}
		
		var id = null;
		if (element[primary]) {
			id = element[primary];
			knex(table).where(primary, id).then(function(result) {
				if (!_.isEmpty(result))	{
					delete element[primary];
					knex(table).where(primary, id).update(element).then(function(result) {
						callbackId(id);
					}, function(result) {
						console.log("**DB: ERROR! %j", result);
					});
				} else {
					knex(table).insert(element).returning(primary).then(function(result) {
						console.log(result);
						id = result[0];
						callbackId(id);
					}, function(result) {
						console.log("**DB: ERROR! %j", result);
					});
				}
			});	
		} else {
			knex(table).insert(element).returning(primary).then(function(result) {
				id = result[0];
				callbackId(id);
			}, function(result) {
				console.log("**DB: ERROR! %j", result);
			});
		}
	};
	
	this.saveTrack = function(track) {
		var that = this;
		var t = {};
		t.id = track.id;
		t.name = track.name;
		t.lanes_json = JSON.stringify(track.lanes);
		this.save('track', t);
		
		this.removePiecesFromTrack(track.id).then(function() {
			_.each(track.pieces, function(piece) {
				that.savePiece(track.id, piece);
			}, function(result) {
				console.log("**DB: ERROR! %j", result);
			});
		});
	};
	
	this.savePiece = function(trackId, piece) {
		piece.track_id = trackId;
		this.save('track_piece', piece);
	};
	
	this.removePiecesFromTrack = function(trackId) {
		return knex('track_piece').where('track_id', trackId).del();
	};

	this.saveRun = function(trackId, runId) {
		var run = {};

		if (!runId) {
			runId = _.now();
		}
		run.id = runId;
		run.track_id = trackId;
		this.save('run', run, 'id');
		return runId;
	};

	this.saveHistory = function(carPosition, runId) {
		var that = this;
		knex('track_piece').where('index', carPosition.pieceIndex).then(function(result) {
			if (result.length == 0) {
				return;
			} 

			var cp = {};
			cp.angle = carPosition.angle;
			cp.piece_distance = carPosition.pieceDistance;
			cp.start_lane = carPosition.startLaneIndex;
			cp.end_lane = carPosition.endLaneIndex;
			cp.lap = carPosition.lap;
			cp.tick = carPosition.tick;
			cp.piece_id = result[0].id;
			cp.run_id = runId;
			
			that.save('run_history', cp);
		});
	};
};