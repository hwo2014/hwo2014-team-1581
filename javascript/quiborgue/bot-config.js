var fs = require("fs");
var _ = require("underscore");

module.exports = function() {
	var base = __dirname + "/config/bot-conf";
	if (fs.existsSync(base + ".json")) {
		var content = JSON.parse(fs.readFileSync(base + ".json", "utf-8"));
	} else {
		var content = JSON.parse(fs.readFileSync(base + "-prod.json", "utf-8"));
	}
	

	this.get = function(name) {
		if (_.has(content, name)) {
			return content[name];
		}
		throw "Configuração " + name + " não encontrada.";
	}
}