var _ = require("underscore");
var Calculus = require("../bot-calculus");
var Helper = require("../bot-helper");

function clone(a) {
   return JSON.parse(JSON.stringify(a));
}

module.exports = function(chatterbox) {
	// Variáveis auxilares
	this.chatterbox = chatterbox;
	this.calculus = new Calculus();
	this.helper = new Helper(chatterbox);
	
	// Histórico de movimentação
	this.history = [];
	
	// Tempos de voltas completas
	this.fullLapTimes = [];

	// Distâncias
	this.distance = 0.0;
	this.prevDistance = 0.0;

	// Velocidades
	this.speed = 0.0;
	this.prevSpeed = 0.0;

	// Aceleração atual
	this.aceleration = 0.0;

	// Car Position atual
	this.position = null;

	// Carro possui turbo
	this.hasTurbo = false;
	this.usedTurbo = false;

	// Combinação de lanes possíveis
	this.ways = [];

	// Variáveis de inicialização
	this.askedSwitch = 0;
	this.started = false;

	/********************
	 * BEFORE RACE
	 ********************/
	this.refreshPossibleWays = function(lane) {
		this.calculateDistance(0,lane);
		
		this.ways = _.sortBy(this.ways, function(way) {
			return way.distance;
		});
	};

	/**
	 * Retorna a maior reta da pista
	 */
	this.findBigStraightStrip = function() {
		var currentSize = 0;
		var maxSize = 0;
		var lastPiece = 0;

		var pieces = this.helper.getTrack().pieces;
		for (var i = 0; i < pieces.length * 2; i++) {
			var piece = this.helper.getPiece(i);
			if (!piece.angle) {
				currentSize++;
			} else {
				currentSize = 0;
			}

			if (currentSize > maxSize) {
				lastPiece = i;
				maxSize = currentSize;
			}
		}

		return this.helper.getPiece(lastPiece - maxSize + 1).index;
	}

	/**
	 * Calcula distância a partir de uma parte da pista considerando,
	 * informando a atual posição na lane. Caso exista um caminho prévio
	 * a ser somado (mais utilizado na recursividade da função).
	 */
	this.calculateDistance = function(trackPieceIndex, lane, way) {
		// Caso não seja informado um caminho corrente então cria um novo
		if (!way) {
			way = {
				distance: 0.0,
				pieces: []
			};		
		}
		
		// Se extrapolou o tamanho da pista então adiciona aos caminhos
		// possíveis e retorna
		if (trackPieceIndex === this.helper.getTrack().pieces.length) {
			this.ways.push(way);
			return 0.0;
		}
		
		// Obtem peça atual
		var piece = this.helper.getPiece(trackPieceIndex);

		// Adiciona pedaço da pista e lane atual
		var p = {
			index: trackPieceIndex,
			lane: lane
		};
		way.pieces.push(p);
		
		way.distance += this.calculus.trackLength(this.helper.getPiece(trackPieceIndex), this.helper.getLane(lane).distanceFromCenter);
		p.distanceSoFar = way.distance;
		if (piece.switch) {
			if ((lane + 1) < this.helper.getTrack().lanes.length) {
				p.switch = "Right";
				var way1 = clone(way);
				delete p.switch;
				
				// Acréscimo por switch
				way1.distance += 10;
				this.calculateDistance(trackPieceIndex + 1, lane + 1, way1);
			}
			
			if ((lane - 1) >= 0) {
				p.switch = "Left";
				var way2 = clone(way);
				delete p.switch;
				
				// Acréscimo por switch
				way2.distance += 10;
				this.calculateDistance(trackPieceIndex + 1, lane - 1, way2);
			}
		}
		
		this.calculateDistance(trackPieceIndex + 1, lane, way);
		
		return way.distance;
	};

	this.turboIfCan = function() {
		if (this.getNearCarPosition(0).tick > (this.usedTurbo + 30)) {
			this.usedTurbo = false;
		}

		if (this.usedTurbo !== false) return false;
		if (!this.hasTurbo) return false;
		if (this.findBigStraightStrip() != this.position.pieceIndex) return false;

		this.usedTurbo = this.getNearCarPosition(0).tick;
		this.chatterbox.turbo();
		return true;
	}

	this.changeLaneIfNeeded = function() {
		var way = this.ways[0];
		var that = this;
		
		// Verifica se deve mudar de pista no próximo pedaço.
		var r = _.find(way.pieces, function(piece) {
			return (piece.index === (that.position.pieceIndex + 1)) && piece.switch;
		});
		
		// Gambiarra para mandar solicitação de mudança com certa frequencia.
		// Em alguns casos o comando não estava sendo esquecido pelo servidor.
		this.askedSwitch = (this.askedSwitch % 5);
		
		if (r) {
			if (!this.askedSwitch) {
				this.chatterbox.switchLane(r.switch);
				this.askedSwitch++;
				return true;
			}
			this.askedSwitch++;
		} else {
			this.askedSwitch = 0;
		}
		
		r = null;
		return false;
	}

	/********************
	 * ON MOVE FUNCTIONS
	 ********************/
	this.updateVars = function() {
		// Primeira coisa é adicionar item no histórico para funções 'Near' funcionarem
		this.history.push(this.helper.getMyCarPosition());

		this.position = this.getNearCarPosition(0);
		
		this.prevDistance = this.distance;
		this.distance = this.getNearDistance(0);

		this.prevSpeed = this.speed;
		this.speed = this.distance - this.prevDistance;

		this.aceleration = this.speed - this.prevSpeed;
	};

	this.printDebugInfo = function() {
		this.printTrackPiece(this.position.pieceIndex);
		this.printCarPosition(this.position);
	};

	this.react = function() {
		if (!this.changeLaneIfNeeded() && !this.turboIfCan()) {
			var piece1 = this.helper.getPiece((this.position.pieceIndex + 1));
			var curveSpeed = this.calculus.curveSpeed(piece1, this.position.startLaneIndex)/13.0;
			
			if (this.speed > curveSpeed) {
				this.chatterbox.throttle(0.0);
			} else {
				this.chatterbox.throttle(1.0);
			}
		}
	};

	/*******************
	 * DEBUG INFO
	 *******************/
	this.printTrackPiece = function(index) {
		var trackPiece = this.helper.getPiece(index);
		var track = this.helper.getTrack();
		var that = this;
		
		console.log("< track piece #%d; length: %d; radius: %d; angle: %d; switch?: %s; ", 
			index, this.calculus.trackLength(trackPiece), trackPiece.radius, trackPiece.angle, trackPiece.switch);

		_.each(track.lanes, function(lane, i) {
			var laneDistance = lane.distanceFromCenter;
			laneDistance *= -trackPiece.angle.sign();
			console.log("< track piece #%d lane #%d; length: %d; radius: %d;",
			index, i, that.calculus.trackLength(trackPiece, lane.distanceFromCenter), trackPiece.radius + laneDistance);
		});
	};
	
	this.printCarPosition = function(carPosition) {
		console.log("< car position #%d; speed: %d; aceleration: %d; angle: %d; lap: %d; lane: %d -> %d; piece distance: %d; total distance: %d;", 
			carPosition.pieceIndex, this.speed, this.aceleration, carPosition.angle, carPosition.lap, 
			carPosition.startLaneIndex, carPosition.endLaneIndex, carPosition.pieceDistance, this.distance);
	};

	this.printPossibleWays = function() {
		var that = this;
		_.find(this.ways, function(way) {
			that.printWay(way);
		});
	}
	
	this.printWay = function(way) {
		console.log("%j", way);
		console.log("distance: %d; pieces: %d", way.distance, way.pieces.length);
		_.each(way.pieces, function(piece) {
			if (piece.switch) {
				console.log("piece %d; switch: %s; distance: %d", piece.index, piece.switch, piece.distanceSoFar);
			}
		});
		console.log("----------------------------------------------");
	}

	this.printCurves = function() {
		var that = this;
		_.each(this.helper.getTrack().pieces, function(piece, index) {
			if (piece.angle) {
				console.log("#%d - %do %d = %d", index, piece.angle, piece.radius, that.calculus.curveSpeed(piece, 0));
			}	
		});
	}
	
	this.printLapTimes = function() {
		var fullLapTimes = this.calculus.fullLapTimes(this.history);
		
		_.each(fullLapTimes, function(lapTime, index) {
			console.log('Lap %d: %d', index, lapTime.toFixed(3));
		});
	}

	/*******************
	 * HELPERS
	 *******************/
	/**
	 * Obtem a posição do carro no histórico
	 */
	this.getHistory = function(index) {
		return this.history[index];
	};
	
	/**
	 * Obtem pedaço da pista da localizacao atual do carro + diff, ou seja
	 * caso diff seja 1 então retorna o próximo pedaço da pista
	 * caso diff seja -1 então retorna o upedaço passado da pista
	 * diff pode variar entre -infinito a +infinito
	 */
	this.getNearPiece = function(diff) {
		return this.helper.getPiece((this.getNearCarPosition(diff).pieceIndex) % this.helper.getTrack().pieces.length);
	};
	
	/**
	 * Obtem a posição do carro da localizacao atual do carro + diff, ou seja
	 * não suporta valores positivos (no futuro), por enquanto
	 * caso diff seja -1 então retorna a posição anterior
	 * diff pode variar entre -infinito a 0
	 */
	this.getNearCarPosition = function(diff) {
		return this.getHistory((this.history.length - 1) + diff);
	};
	
	/**
	 * Obtem a distância percorrida até o ponto atual do carro + diff, ou seja
	 * não suporta valores positivos (no futuro), por enquanto
	 * caso diff seja -1 então retorna a distância percorrida até a peça passada
	 * diff pode variar entre -infinito a +infinito
	 */
	this.getNearDistance = function(diff) {
		// Se não possui movimentos então simplesmente retorna 0.0 de distância
		if (this.history.length === 0) {
			return 0.0;
		}
		
		// Se a quantidade a voltar estrapolou o tamanho do histórico então quer dizer que
		// voltamos demais e por isso deve-se retornar 0.
		if (Math.abs(diff) >= this.history.length) {
			return 0.0;
		}
		
		// Se quisermos saber o quanto o carro andou na posição atual devemos pegar o tanto que
		// o carro andou na posição anterior e somar com o tanto que ele andou nessa peça.
		if (diff === 0) {
			return this.getNearDistance(diff - 1) + this.getNearCarPosition(0).pieceDistance;
		}

		var carPosition = this.getNearCarPosition(diff);
		var carPosition1 = this.getNearCarPosition(diff + 1);

		// Para evitar somar a distância percorrida na mesma peça duas vezes
		if (carPosition.pieceIndex === carPosition1.pieceIndex) {
			return this.getNearDistance(diff - 1);
		}
		
		// A distância percorrida no histórico anterior mais a distância da peça atual, considerando
		// a 'lane' na qual o carrinho estava.
		return this.getNearDistance(diff - 1) + this.calculus.trackLength(this.getNearPiece(diff), 
				this.helper.getLane(carPosition.startLaneIndex).distanceFromCenter);
	};

	/*********************
	 * POLICY
	 *********************/
	this.onDefault = function() {
		this.chatterbox.ping();
	};

	this.onGameInit = function() {
		this.refreshPossibleWays(0);
		// this.printPossibleWays();
		// this.printCurves();
		this.findBigStraightStrip();
		this.chatterbox.ping();
	};
	
	this.onGameStart = function() {
		this.started = true;
	};

	this.onCarPositions = function() {
		if (!this.started) {
			this.chatterbox.ping();
			return;
		}

		this.updateVars();
		// this.printDebugInfo();
		this.react();
	};

	this.onCrash = function() {
		// console.log('policy: car crashed\007\007\007\007\007');
	};

	this.onSpawn = function() {
		// console.log('policy: car spawned\007\007');
	};

	this.onLapFinished = function() {
		// console.log('lap: lap done');
	};

	this.onFinish = function() {
		// console.log('race finished');
		// console.log('lap times:');
		// this.printLapTimes();
	};

	this.onGameEnd = function() {
		// console.log('game end');
	};

	this.onTurboAvailable = function() {
		this.hasTurbo = true;
	};
};