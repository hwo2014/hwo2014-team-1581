// Usar essa classe como base para criar listeners.
// Lembre-se que as informações estão todas no this.chatterbox
// Para um exemplo mais completo veja: capim-policy.js

var _ = require("underscore");
var Calculus = require("../bot-calculus");
var Helper = require("../bot-helper");

module.exports = function(chatterbox) {
	this.chatterbox = chatterbox;
	this.calculus = new Calculus();
	this.helper = new Helper(chatterbox);

	this.onDefault = function(caller) {
		console.log("< ** could not find %s **", caller);
	};
	
	this.onCreateRace = function() {
		console.log("< ** created race");
	};

	this.onYourCar = function() {
		console.log("< ** your car");
	};

	this.onGameInit = function() {
		console.log("< ** game init");
	};

	this.onTurboAvailable = function() {
		console.log("< ** turbo available");
	}
	
	this.onGameStart = function() {
		console.log("< ** game start");
	};

	this.onCarPositions = function() {
		console.log("< ** car positions");
	};

	this.onCrash = function() {
		console.log("< ** crashed");
	};

	this.onSpawn = function() {
		console.log("< ** spawned");
	};

	this.onLapFinished = function() {
		console.log("< ** lap finished");
	};

	this.onFinish = function() {
		console.log("< ** race finished");
	};

	this.onGameEnd = function() {
		console.log("< ** game ended");
	};

	this.onTournamentEnd = function() {
		console.log("< ** tournament ended");
	};

	this.onDnf = function() {
		console.log("< ** disqualified");
	}
};
