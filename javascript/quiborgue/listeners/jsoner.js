var _ = require("underscore");
var fs = require("fs");
var Calculus = require("../bot-calculus");
var Helper = require("../bot-helper");

function clone(a) {
   return JSON.parse(JSON.stringify(a));
}

module.exports = function(chatterbox) {
	// JSON file
	this.angletickjson = [];
	this.speedtickjson = [];

	this.maxspeed = 0;

	// Output file
	this.speedtick = "quiborgue/viewer/json/speedtick" + _.now() + ".json";
	this.angletick = "quiborgue/viewer/json/angletick" + _.now() + ".json";

	// Variáveis auxilares
	this.chatterbox = chatterbox;
	this.calculus = new Calculus();
	this.helper = new Helper(chatterbox);
	
	// Histórico de movimentação
	this.history = [];
	
	// Distâncias
	this.distance = 0.0;
	this.prevDistance = 0.0;

	// Velocidades
	this.speed = 0.0;
	this.prevSpeed = 0.0;

	// Aceleração atual
	this.aceleration = 0.0;

	// Car Position atual
	this.position = null;


	/********************
	 * ON MOVE FUNCTIONS
	 ********************/
	this.updateVars = function() {
		// Primeira coisa é adicionar item no histórico para funções 'Near' funcionarem
		this.history.push(this.helper.getMyCarPosition());

		this.position = this.getNearCarPosition(0);
		
		this.prevDistance = this.distance;
		this.distance = this.getNearDistance(0);

		this.prevSpeed = this.speed;
		this.speed = this.distance - this.prevDistance;

		this.aceleration = this.speed - this.prevSpeed;
	};

	this.react = function() {
		this.doAngletick(); 
		this.doSpeedtick();
	};

	this.doAngletick = function() {
		this.angletickjson.push({
			time: this.position.tick,
			name: "speed",
			value: this.position.angle
		});

		fs.writeFileSync(this.angletick, JSON.stringify(this.angletickjson));
	}

	this.doSpeedtick = function() {
		this.speedtickjson.push({
			time: this.position.tick,
			name: "speed",
			value: this.speed
		});
		fs.writeFileSync(this.speedtick, JSON.stringify(this.speedtickjson));
	}

	/*******************
	 * HELPERS
	 *******************/
	/**
	 * Obtem a posição do carro no histórico
	 */
	this.getHistory = function(index) {
		return this.history[index];
	};
	
	/**
	 * Obtem pedaço da pista da localizacao atual do carro + diff, ou seja
	 * caso diff seja 1 então retorna o próximo pedaço da pista
	 * caso diff seja -1 então retorna o upedaço passado da pista
	 * diff pode variar entre -infinito a +infinito
	 */
	this.getNearPiece = function(diff) {
		return this.helper.getPiece((this.getNearCarPosition(diff).pieceIndex) % this.helper.getTrack().pieces.length);
	};
	
	/**
	 * Obtem a posição do carro da localizacao atual do carro + diff, ou seja
	 * não suporta valores positivos (no futuro), por enquanto
	 * caso diff seja -1 então retorna a posição anterior
	 * diff pode variar entre -infinito a 0
	 */
	this.getNearCarPosition = function(diff) {
		return this.getHistory((this.history.length - 1) + diff);
	};
	
	/**
	 * Obtem a distância percorrida até o ponto atual do carro + diff, ou seja
	 * não suporta valores positivos (no futuro), por enquanto
	 * caso diff seja -1 então retorna a distância percorrida até a peça passada
	 * diff pode variar entre -infinito a +infinito
	 */
	this.getNearDistance = function(diff) {
		// Se não possui movimentos então simplesmente retorna 0.0 de distância
		if (this.history.length === 0) {
			return 0.0;
		}
		
		// Se a quantidade a voltar estrapolou o tamanho do histórico então quer dizer que
		// voltamos demais e por isso deve-se retornar 0.
		if (Math.abs(diff) >= this.history.length) {
			return 0.0;
		}
		
		// Se quisermos saber o quanto o carro andou na posição atual devemos pegar o tanto que
		// o carro andou na posição anterior e somar com o tanto que ele andou nessa peça.
		if (diff === 0) {
			return this.getNearDistance(diff - 1) + this.getNearCarPosition(0).pieceDistance;
		}

		var carPosition = this.getNearCarPosition(diff);
		var carPosition1 = this.getNearCarPosition(diff + 1);

		// Para evitar somar a distância percorrida na mesma peça duas vezes
		if (carPosition.pieceIndex === carPosition1.pieceIndex) {
			return this.getNearDistance(diff - 1);
		}
		
		// A distância percorrida no histórico anterior mais a distância da peça atual, considerando
		// a 'lane' na qual o carrinho estava.
		return this.getNearDistance(diff - 1) + this.calculus.trackLength(this.getNearPiece(diff), 
				this.helper.getLane(carPosition.startLaneIndex).distanceFromCenter);
	};

	/*********************
	 * POLICY
	 *********************/
	this.onDefault = function() {
		this.chatterbox.ping();
	};

	this.onGameInit = function() {
		this.chatterbox.ping();
	};
	
	this.onGameStart = function() {
		this.started = true;
	};

	this.onCarPositions = function() {
		if (!this.started) {
			this.chatterbox.ping();
			return;
		}

		this.updateVars();
		this.react();
	};
};