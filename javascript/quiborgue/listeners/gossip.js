// Usar essa classe como base para criar políticas.
// Lembre-se que as informações estão todas no this.chatterbox
// Para um exemplo mais completo veja: capim-policy.js

module.exports = function(chatterbox) {
	this.chatterbox = chatterbox;

	this.onDefault = function() {
		console.log("gossip listener: %j", this.chatterbox.msg);
	};

	this.sent = function(json) {
		console.log("gossip listener: %j", json);
	};

};
